# Pour compiler le projet #

Dans une invite de commande, faire:

```
#!java

gradlew.bat build
```
Cela va :

* télécharger Gradle 2.0 (cette version précisément comme défini dans le fichier de build) si pas encore fait
* Récupérer les dépendances depuis le repo Maven
* Compiler les sources
* Créer un JAR
* Exécuter les tests unitaires

# Pour utiliser le projet dans Eclipse #

Dans une invite de commande, faire:

```
#!java

gradlew.bat eclipse
```
Cela va :

* Télécharger Gradle 2.0 (si pas encore réalisé)
* Récupérer les dépendances (en dehors du projet)
* Créer ou mettre à jour les fichiers de configuration du projet Eclipse (références vers des jars, etc.)

# Build on Jenkins #

This project is build on Jenkins at this URL: http://192.168.31.13:8080/jenkins/job/Gradle-test/

Gradle is not installed on the machine, it resorts to the mechanism explain above in order to automatically download the right Gradle version.