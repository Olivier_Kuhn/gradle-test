import org.junit.Test;

public class StringHelperTest {

	@Test
	public void testSplit(){
		String s = "a,b,c";
		String[] expected = {"a","b","c"};
		
		String[] res = StringHelper.split(s);
		
		org.junit.Assert.assertArrayEquals(expected, res);
	}
	
}
