
import org.apache.commons.lang.StringUtils;

public class StringHelper {

	public static String[] split(String s) {
		if (s == null) {
			return null;
		}
		return StringUtils.split(s, ",");
	}

}
